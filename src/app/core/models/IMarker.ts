export default interface Marker {
    id: String;
    lat: Number;
    long: Number;
    title: String;
    content: String;
    image_url: String;
}
