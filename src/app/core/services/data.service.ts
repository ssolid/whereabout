import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {catchError, map} from 'rxjs/operators';
import Marker from '../models/IMarker';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {
  }

  getData(): Observable<Marker[]> {
    return this.httpClient.get<Marker[]>('https://wf-challenge-ceanyth.herokuapp.com/posts').pipe(
      map(res => {
        return res;
      })
    );
  }

  saveData(newMarker): Observable<Marker> {
    return this.httpClient.post<Marker>('https://wf-challenge-ceanyth.herokuapp.com/posts', {
      'post' : newMarker
    });
  }

  editData(newMarker): Observable<Marker> {
    return this.httpClient.put<Marker>('https://wf-challenge-ceanyth.herokuapp.com/posts/' + newMarker.id, {
      'post' : newMarker
    });
  }

}
