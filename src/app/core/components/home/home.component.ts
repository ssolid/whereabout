import {Component, OnInit, ViewChild} from '@angular/core';
import Marker from 'src/app/core/models/IMarker';
import {DataService} from '../../services/data.service';
import {NgForm} from '@angular/forms';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // init barcelona coordinates
  lat = 41.390205;
  long = 2.154007;

  markers = [];
  markersInit = [];
  isCreationMode = false;
  isEditMode = false;

  newMarker = {
    id: '',
    long: '',
    lat: '',
    image_url: '',
    title: '',
    content: ''
  };

  @ViewChild('f') public userFrm: NgForm;


  readonly MARKER_BLUE = 'assets/icons8-marker-40.png';
  readonly MARKER_RED = 'assets/icons8-marker-40-red.png';
  private readonly notifier: NotifierService;

  constructor(private dataService: DataService, private notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.dataService.getData().subscribe((data: Array<Marker>) => {
      data.forEach(marker => {
        this.markers.push({
          id: marker.id,
          lat: Number(marker.lat),
          long: Number(marker.long),
          title: marker.title,
          content: marker.content,
          image_url: marker.image_url,
          iconUrl: this.MARKER_BLUE,
          markerDraggable: false
        });
      });
      this.markersInit = JSON.parse(JSON.stringify(this.markers));
    });
    this.notifier.notify( 'info', 'Double cLick on the map to add a new address !' );

  }


  public onChooseLocation(event) {
    this.isCreationMode = true;
    this.newMarker = {
      id: null,
      lat:null,
      long:null,
      image_url: null,
      title: null,
      content: null
    };
    this.markers = this.markers.filter(m => m.id !== 'new');
    this.markers.push({
      id: 'new',
      lat: event.coords.lat,
      long: event.coords.lng,
      iconUrl: this.MARKER_RED,
      markerDraggable: true
    })
    this.newMarker.long = event.coords.lng;
    this.newMarker.lat = event.coords.lat;
    console.log(this.newMarker);
  }

  public returnListMode() {
    this.deleteNewPosition();
    this.isCreationMode = false;
    this.isEditMode = false;
    this.markers.forEach(m => {
      m.iconUrl = this.MARKER_BLUE;
    });
    this.markers = JSON.parse(JSON.stringify(this.markersInit));
  }

  public saveNewPosition() {
    if(!this.userFrm.valid){
     return;
    }
    this.deleteNewPosition();
    this.dataService.saveData(this.newMarker).subscribe((data: Marker) => {
      this.dataService.getData().subscribe((data: Array<Marker>) => {
        data.forEach(marker => {
          this.markers.push({
            id: marker.id,
            lat: Number(marker.lat),
            long: Number(marker.long),
            title: marker.title,
            content: marker.content,
            image_url: marker.image_url,
            iconUrl: this.MARKER_BLUE,
            markerDraggable: false
          });
        });
        this.isCreationMode = false;
        this.isEditMode = false;
        this.notifier.notify( 'success', 'The location has been created successfully :)!' );

      });
    });
  }

  public newDraggPosition(event) {
    console.log(event);
    this.newMarker.long = event.coords.lng;
    this.newMarker.lat = event.coords.lat;
  }

  public deleteNewPosition() {
    this.markers = this.markers.filter(m => m.id !== 'new'); // delete the new position
  }

  public editMarker(marker) {
    this.markers.forEach(m => {
      if (m.id === marker.id) {
        m.markerDraggable = true;
        m.iconUrl = this.MARKER_RED;
      }
    });
    this.isEditMode = true;
    this.newMarker = marker;

  }

  public updatePosition () {
    if(!this.userFrm.valid){
      return;
    }
    this.dataService.editData(this.newMarker).subscribe((data: Marker) => {
      this.dataService.getData().subscribe((data: Array<Marker>) => {
        this.markers = [];
        data.forEach(m => {
          this.markers.push({
            id: m.id,
            lat: Number(m.lat),
            long: Number(m.long),
            title: m.title,
            content: m.content,
            image_url: m.image_url,
            iconUrl: this.MARKER_BLUE,
            markerDraggable: false
          });
        });
        this.notifier.notify( 'success', 'The location has been updated successfully :)!' );
        this.isEditMode = false;
      });
    });
  }

}
